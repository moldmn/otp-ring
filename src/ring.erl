-module(ring).

-author("Dmitry Nazarov").

-behaviour(application).

%% Application callbacks
-export([start/3, start/2, stop/1]).

%%======================================
%% application callback functions
%%======================================
start(_Type, _Args) ->
    ring_sup:start_link({5, 5, "test"})
.

start(N, M, Message) ->
    ring_sup:start_link({N, M, Message})
.

stop(_State) ->
    ok
.
