-module(worker_srv).

-behaviour(gen_server).

-record(state, {
	name, %name of the current worker
	next_worker, % name of next worker
	messages_left % count of messages left 
}).

%% ------------------------------------------------------------------
%% API Function Exports
%% ------------------------------------------------------------------
-export([start_link/1]).

-export([send_message/2]).
%% ------------------------------------------------------------------
%% gen_server Function Exports
%% ------------------------------------------------------------------
-export([init/1, handle_call/3, handle_cast/2, handle_info/2,
         terminate/2, code_change/3]).

%% ------------------------------------------------------------------
%% API Function Definitions
%% ------------------------------------------------------------------

start_link({WorkerName, _Next, _M} = Args) ->
    gen_server:start_link({local, WorkerName}, ?MODULE, Args, [])
.

send_message(WorkerName, Message) ->
	case whereis(WorkerName) of 
	undefined ->
		catch exit(whereis(ring_sup),kill)
	;
	_Pid ->
		gen_server:cast(WorkerName, {message, Message})	
	end
.


%% ------------------------------------------------------------------
%% gen_server Function Definitions
%% ------------------------------------------------------------------

init({Name, Next, M}) ->
    process_flag(trap_exit, true),
    {ok, #state{name = Name, next_worker = Next, messages_left = M}}
.

handle_call(_Request, _From, State) ->
    {reply, ok, State}
.

handle_cast({message, Message}, State) ->
    error_logger:info_msg("worker ~p got message: ~p", [State#state.name, Message]),
    send_message(State#state.next_worker, Message),
    MessagesLeft = State#state.messages_left - 1,
    case MessagesLeft of 
    0 ->
		error_logger:info_msg("stop worker ~p", [State#state.name]),
		{stop, normal, State}
    ;
    _ ->
		NewState = State#state{messages_left = MessagesLeft},
		{noreply, NewState}
	end
;
handle_cast(_Msg, State) ->
    {noreply, State}
.

handle_info(_Info, State) ->
    {noreply, State}
.

terminate(_Reason, _State) ->
   ok
.

code_change(_OldVsn, State, _Extra) ->
    {ok, State}
.

%% ------------------------------------------------------------------
%% Internal Function Definitions
%% ------------------------------------------------------------------


