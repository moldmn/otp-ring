-module(ring_sup).

-behaviour(supervisor).

%% API
-export([start_link/1, stop/0]).

%% Supervisor callbacks
-export([init/1]).

%% Helper macro for declaring children of supervisor
-define(CHILD(I, Type, Args), {I, {worker_srv, start_link, Args}, transient, 5000, Type, [I]}).

%% Helper macro for worker name
-define(WORKER_NAME(Number), list_to_atom(integer_to_list(Number))).

%% ===================================================================
%% API functions
%% ===================================================================

start_link({N, M, Message}) ->
    Res = supervisor:start_link({local, ?MODULE}, ?MODULE, {N,M}),
    worker_srv:send_message(?WORKER_NAME(1),Message),
    Res
.

stop() ->
  ok
.

%% ===================================================================
%% Supervisor callbacks
%% ===================================================================

init({N, M}) ->
    WorkerNames = [{Number, ?WORKER_NAME(Number)}|| Number <- lists:seq(1,N)],
    
    Childs = [?CHILD(WorkerName, worker, [{WorkerName, getNext(Number, WorkerNames), M}]) || {Number, WorkerName} <- WorkerNames],
    
    {ok, { {one_for_one, 5, 10}, Childs} }
.

%% ===================================================================
%% Private functions
%% ===================================================================


getNext(Number,WorkerNames) when Number == length(WorkerNames) ->
	element(2,lists:nth(1, WorkerNames))
;
getNext(Number,WorkerNames) ->
	element(2,lists:nth(Number + 1, WorkerNames))
.
